﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Helpandtips
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Helpandtips))
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.Panel30 = New System.Windows.Forms.Panel()
        Me.Panel33 = New System.Windows.Forms.Panel()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.Panel34 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel32 = New System.Windows.Forms.Panel()
        Me.Panel35 = New System.Windows.Forms.Panel()
        Me.Panel36 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Panel37 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel39 = New System.Windows.Forms.Panel()
        Me.Panel40 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel31 = New System.Windows.Forms.Panel()
        Me.Panel45 = New System.Windows.Forms.Panel()
        Me.Panel46 = New System.Windows.Forms.Panel()
        Me.Panel38 = New System.Windows.Forms.Panel()
        Me.Panel41 = New System.Windows.Forms.Panel()
        Me.Panel42 = New System.Windows.Forms.Panel()
        Me.Panel43 = New System.Windows.Forms.Panel()
        Me.Panel44 = New System.Windows.Forms.Panel()
        Me.Panel9.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.Panel25.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel28.SuspendLayout()
        Me.Panel26.SuspendLayout()
        Me.Panel33.SuspendLayout()
        Me.Panel32.SuspendLayout()
        Me.Panel36.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel37.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel39.SuspendLayout()
        Me.Panel31.SuspendLayout()
        Me.Panel45.SuspendLayout()
        Me.Panel41.SuspendLayout()
        Me.Panel43.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.White
        Me.Panel9.Controls.Add(Me.Panel10)
        Me.Panel9.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel9.Location = New System.Drawing.Point(5, 564)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(853, 5)
        Me.Panel9.TabIndex = 10
        '
        'Panel10
        '
        Me.Panel10.Location = New System.Drawing.Point(0, 435)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(5, 5)
        Me.Panel10.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Panel8)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(5, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(853, 34)
        Me.Panel1.TabIndex = 7
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox2.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.MinimizeNew
        Me.PictureBox2.Location = New System.Drawing.Point(791, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(28, 28)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.CloseNew
        Me.PictureBox1.Location = New System.Drawing.Point(822, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(28, 28)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(853, 5)
        Me.Panel8.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(375, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Help and Tips"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(858, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(5, 569)
        Me.Panel2.TabIndex = 8
        '
        'Panel4
        '
        Me.Panel4.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.Panel4.Location = New System.Drawing.Point(0, 564)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(5, 5)
        Me.Panel4.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(5, 5)
        Me.Panel3.TabIndex = 2
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Controls.Add(Me.Panel6)
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(5, 569)
        Me.Panel5.TabIndex = 9
        '
        'Panel6
        '
        Me.Panel6.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me.Panel6.Location = New System.Drawing.Point(0, 564)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(5, 5)
        Me.Panel6.TabIndex = 3
        '
        'Panel7
        '
        Me.Panel7.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(5, 5)
        Me.Panel7.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(27, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(247, 49)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Help and Tips"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(32, 113)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(763, 22)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Here, you can learn more about the features in Zuaro OS and how to use them to he" & _
    "lp you use Zuaro OS."
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel11.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.Panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel11.Controls.Add(Me.Panel15)
        Me.Panel11.Controls.Add(Me.Panel14)
        Me.Panel11.Controls.Add(Me.Panel12)
        Me.Panel11.Location = New System.Drawing.Point(49, 158)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(201, 146)
        Me.Panel11.TabIndex = 13
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Transparent
        Me.Panel15.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel15.Location = New System.Drawing.Point(90, 0)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(21, 10)
        Me.Panel15.TabIndex = 2
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.White
        Me.Panel14.Location = New System.Drawing.Point(3, 73)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(67, 59)
        Me.Panel14.TabIndex = 1
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Panel12.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel12.Controls.Add(Me.Panel13)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel12.Location = New System.Drawing.Point(0, 135)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(201, 11)
        Me.Panel12.TabIndex = 0
        '
        'Panel13
        '
        Me.Panel13.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel13.Location = New System.Drawing.Point(0, 0)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(16, 11)
        Me.Panel13.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(35, 309)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 22)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Open an App or Windows App"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(331, 309)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(199, 22)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Change Zuaro OS settings"
        '
        'Panel16
        '
        Me.Panel16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel16.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.Panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel16.Controls.Add(Me.Panel17)
        Me.Panel16.Controls.Add(Me.Panel18)
        Me.Panel16.Controls.Add(Me.Panel19)
        Me.Panel16.Location = New System.Drawing.Point(329, 158)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(201, 146)
        Me.Panel16.TabIndex = 15
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.Transparent
        Me.Panel17.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel17.Location = New System.Drawing.Point(90, 0)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(21, 10)
        Me.Panel17.TabIndex = 2
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.White
        Me.Panel18.Controls.Add(Me.Panel22)
        Me.Panel18.Controls.Add(Me.Panel21)
        Me.Panel18.Location = New System.Drawing.Point(44, 25)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(102, 85)
        Me.Panel18.TabIndex = 1
        '
        'Panel22
        '
        Me.Panel22.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Panel22.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel22.Location = New System.Drawing.Point(26, 0)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(76, 10)
        Me.Panel22.TabIndex = 1
        '
        'Panel21
        '
        Me.Panel21.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Panel21.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel21.Location = New System.Drawing.Point(0, 0)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(26, 85)
        Me.Panel21.TabIndex = 0
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.Transparent
        Me.Panel19.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel19.Controls.Add(Me.Panel20)
        Me.Panel19.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel19.Location = New System.Drawing.Point(0, 135)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(201, 11)
        Me.Panel19.TabIndex = 0
        '
        'Panel20
        '
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel20.Location = New System.Drawing.Point(0, 0)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(16, 11)
        Me.Panel20.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(612, 309)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(183, 22)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Get apps from the store"
        '
        'Panel23
        '
        Me.Panel23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel23.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.Panel23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel23.Controls.Add(Me.Panel24)
        Me.Panel23.Controls.Add(Me.Panel25)
        Me.Panel23.Controls.Add(Me.Panel28)
        Me.Panel23.Location = New System.Drawing.Point(610, 158)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(201, 146)
        Me.Panel23.TabIndex = 17
        '
        'Panel24
        '
        Me.Panel24.BackColor = System.Drawing.Color.Transparent
        Me.Panel24.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel24.Location = New System.Drawing.Point(90, 0)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(21, 10)
        Me.Panel24.TabIndex = 2
        '
        'Panel25
        '
        Me.Panel25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel25.Controls.Add(Me.PictureBox7)
        Me.Panel25.Location = New System.Drawing.Point(48, 29)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(102, 85)
        Me.Panel25.TabIndex = 1
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox7.Location = New System.Drawing.Point(32, 24)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(37, 34)
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'Panel28
        '
        Me.Panel28.BackColor = System.Drawing.Color.Transparent
        Me.Panel28.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel28.Controls.Add(Me.Panel29)
        Me.Panel28.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel28.Location = New System.Drawing.Point(0, 135)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Size = New System.Drawing.Size(201, 11)
        Me.Panel28.TabIndex = 0
        '
        'Panel29
        '
        Me.Panel29.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel29.Location = New System.Drawing.Point(0, 0)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Size = New System.Drawing.Size(16, 11)
        Me.Panel29.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(80, 506)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(130, 22)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Change the look"
        '
        'Panel26
        '
        Me.Panel26.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel26.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.Panel26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel26.Controls.Add(Me.Panel30)
        Me.Panel26.Controls.Add(Me.Panel33)
        Me.Panel26.Location = New System.Drawing.Point(49, 355)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(201, 146)
        Me.Panel26.TabIndex = 19
        '
        'Panel30
        '
        Me.Panel30.BackColor = System.Drawing.Color.Transparent
        Me.Panel30.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel30.Location = New System.Drawing.Point(0, 65)
        Me.Panel30.Name = "Panel30"
        Me.Panel30.Size = New System.Drawing.Size(60, 71)
        Me.Panel30.TabIndex = 1
        '
        'Panel33
        '
        Me.Panel33.BackColor = System.Drawing.Color.Transparent
        Me.Panel33.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel33.Controls.Add(Me.Panel27)
        Me.Panel33.Controls.Add(Me.Panel34)
        Me.Panel33.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel33.Location = New System.Drawing.Point(0, 135)
        Me.Panel33.Name = "Panel33"
        Me.Panel33.Size = New System.Drawing.Size(201, 11)
        Me.Panel33.TabIndex = 0
        '
        'Panel27
        '
        Me.Panel27.BackColor = System.Drawing.Color.Transparent
        Me.Panel27.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel27.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel27.Location = New System.Drawing.Point(180, 0)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Size = New System.Drawing.Size(21, 11)
        Me.Panel27.TabIndex = 2
        '
        'Panel34
        '
        Me.Panel34.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel34.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel34.Location = New System.Drawing.Point(0, 0)
        Me.Panel34.Name = "Panel34"
        Me.Panel34.Size = New System.Drawing.Size(16, 11)
        Me.Panel34.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(295, 506)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(268, 22)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Check and manage Zuaro Defender"
        '
        'Panel32
        '
        Me.Panel32.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel32.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.Panel32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel32.Controls.Add(Me.Panel35)
        Me.Panel32.Controls.Add(Me.Panel36)
        Me.Panel32.Controls.Add(Me.Panel39)
        Me.Panel32.Location = New System.Drawing.Point(329, 355)
        Me.Panel32.Name = "Panel32"
        Me.Panel32.Size = New System.Drawing.Size(201, 146)
        Me.Panel32.TabIndex = 21
        '
        'Panel35
        '
        Me.Panel35.BackColor = System.Drawing.Color.Transparent
        Me.Panel35.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel35.Location = New System.Drawing.Point(90, 0)
        Me.Panel35.Name = "Panel35"
        Me.Panel35.Size = New System.Drawing.Size(21, 10)
        Me.Panel35.TabIndex = 2
        '
        'Panel36
        '
        Me.Panel36.BackColor = System.Drawing.Color.White
        Me.Panel36.Controls.Add(Me.PictureBox3)
        Me.Panel36.Controls.Add(Me.Panel37)
        Me.Panel36.Location = New System.Drawing.Point(46, 31)
        Me.Panel36.Name = "Panel36"
        Me.Panel36.Size = New System.Drawing.Size(109, 85)
        Me.Panel36.TabIndex = 1
        '
        'PictureBox3
        '
        Me.PictureBox3.BackgroundImage = CType(resources.GetObject("PictureBox3.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox3.Location = New System.Drawing.Point(0, 20)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(109, 65)
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'Panel37
        '
        Me.Panel37.BackColor = System.Drawing.Color.White
        Me.Panel37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Panel37.Controls.Add(Me.PictureBox4)
        Me.Panel37.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel37.Location = New System.Drawing.Point(0, 0)
        Me.Panel37.Name = "Panel37"
        Me.Panel37.Size = New System.Drawing.Size(109, 20)
        Me.Panel37.TabIndex = 1
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImage = CType(resources.GetObject("PictureBox4.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(14, 14)
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'Panel39
        '
        Me.Panel39.BackColor = System.Drawing.Color.Transparent
        Me.Panel39.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel39.Controls.Add(Me.Panel40)
        Me.Panel39.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel39.Location = New System.Drawing.Point(0, 135)
        Me.Panel39.Name = "Panel39"
        Me.Panel39.Size = New System.Drawing.Size(201, 11)
        Me.Panel39.TabIndex = 0
        '
        'Panel40
        '
        Me.Panel40.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel40.Location = New System.Drawing.Point(0, 0)
        Me.Panel40.Name = "Panel40"
        Me.Panel40.Size = New System.Drawing.Size(16, 11)
        Me.Panel40.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(594, 506)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(230, 44)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "How Zuaro OS works well with" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Windows Apps"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel31
        '
        Me.Panel31.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel31.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.Panel31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel31.Controls.Add(Me.Panel45)
        Me.Panel31.Controls.Add(Me.Panel38)
        Me.Panel31.Controls.Add(Me.Panel41)
        Me.Panel31.Controls.Add(Me.Panel43)
        Me.Panel31.Location = New System.Drawing.Point(610, 355)
        Me.Panel31.Name = "Panel31"
        Me.Panel31.Size = New System.Drawing.Size(201, 146)
        Me.Panel31.TabIndex = 23
        '
        'Panel45
        '
        Me.Panel45.BackColor = System.Drawing.Color.White
        Me.Panel45.Controls.Add(Me.Panel46)
        Me.Panel45.Location = New System.Drawing.Point(48, 35)
        Me.Panel45.Name = "Panel45"
        Me.Panel45.Size = New System.Drawing.Size(142, 85)
        Me.Panel45.TabIndex = 2
        '
        'Panel46
        '
        Me.Panel46.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel46.BackgroundImage = CType(resources.GetObject("Panel46.BackgroundImage"), System.Drawing.Image)
        Me.Panel46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel46.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel46.Location = New System.Drawing.Point(0, 0)
        Me.Panel46.Name = "Panel46"
        Me.Panel46.Size = New System.Drawing.Size(142, 6)
        Me.Panel46.TabIndex = 1
        '
        'Panel38
        '
        Me.Panel38.BackColor = System.Drawing.Color.Transparent
        Me.Panel38.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel38.Location = New System.Drawing.Point(90, 0)
        Me.Panel38.Name = "Panel38"
        Me.Panel38.Size = New System.Drawing.Size(21, 10)
        Me.Panel38.TabIndex = 2
        '
        'Panel41
        '
        Me.Panel41.BackColor = System.Drawing.Color.White
        Me.Panel41.Controls.Add(Me.Panel42)
        Me.Panel41.Location = New System.Drawing.Point(16, 16)
        Me.Panel41.Name = "Panel41"
        Me.Panel41.Size = New System.Drawing.Size(102, 85)
        Me.Panel41.TabIndex = 1
        '
        'Panel42
        '
        Me.Panel42.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Panel42.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel42.Location = New System.Drawing.Point(0, 0)
        Me.Panel42.Name = "Panel42"
        Me.Panel42.Size = New System.Drawing.Size(102, 6)
        Me.Panel42.TabIndex = 1
        '
        'Panel43
        '
        Me.Panel43.BackColor = System.Drawing.Color.Transparent
        Me.Panel43.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel43.Controls.Add(Me.Panel44)
        Me.Panel43.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel43.Location = New System.Drawing.Point(0, 135)
        Me.Panel43.Name = "Panel43"
        Me.Panel43.Size = New System.Drawing.Size(201, 11)
        Me.Panel43.TabIndex = 0
        '
        'Panel44
        '
        Me.Panel44.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel44.Location = New System.Drawing.Point(0, 0)
        Me.Panel44.Name = "Panel44"
        Me.Panel44.Size = New System.Drawing.Size(16, 11)
        Me.Panel44.TabIndex = 0
        '
        'Helpandtips
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(863, 569)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Panel31)
        Me.Controls.Add(Me.Panel32)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel26)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel23)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Panel16)
        Me.Controls.Add(Me.Panel11)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Helpandtips"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Help and Tips"
        Me.Panel9.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.Panel12.ResumeLayout(False)
        Me.Panel16.ResumeLayout(False)
        Me.Panel18.ResumeLayout(False)
        Me.Panel19.ResumeLayout(False)
        Me.Panel23.ResumeLayout(False)
        Me.Panel25.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel28.ResumeLayout(False)
        Me.Panel26.ResumeLayout(False)
        Me.Panel33.ResumeLayout(False)
        Me.Panel32.ResumeLayout(False)
        Me.Panel36.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel37.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel39.ResumeLayout(False)
        Me.Panel31.ResumeLayout(False)
        Me.Panel45.ResumeLayout(False)
        Me.Panel41.ResumeLayout(False)
        Me.Panel43.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents Panel22 As System.Windows.Forms.Panel
    Friend WithEvents Panel21 As System.Windows.Forms.Panel
    Friend WithEvents Panel19 As System.Windows.Forms.Panel
    Friend WithEvents Panel20 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel23 As System.Windows.Forms.Panel
    Friend WithEvents Panel24 As System.Windows.Forms.Panel
    Friend WithEvents Panel25 As System.Windows.Forms.Panel
    Friend WithEvents Panel28 As System.Windows.Forms.Panel
    Friend WithEvents Panel29 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel26 As System.Windows.Forms.Panel
    Friend WithEvents Panel33 As System.Windows.Forms.Panel
    Friend WithEvents Panel27 As System.Windows.Forms.Panel
    Friend WithEvents Panel34 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel32 As System.Windows.Forms.Panel
    Friend WithEvents Panel35 As System.Windows.Forms.Panel
    Friend WithEvents Panel36 As System.Windows.Forms.Panel
    Friend WithEvents Panel37 As System.Windows.Forms.Panel
    Friend WithEvents Panel39 As System.Windows.Forms.Panel
    Friend WithEvents Panel40 As System.Windows.Forms.Panel
    Friend WithEvents Panel30 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel31 As System.Windows.Forms.Panel
    Friend WithEvents Panel38 As System.Windows.Forms.Panel
    Friend WithEvents Panel41 As System.Windows.Forms.Panel
    Friend WithEvents Panel43 As System.Windows.Forms.Panel
    Friend WithEvents Panel44 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel45 As System.Windows.Forms.Panel
    Friend WithEvents Panel46 As System.Windows.Forms.Panel
    Friend WithEvents Panel42 As System.Windows.Forms.Panel
End Class
