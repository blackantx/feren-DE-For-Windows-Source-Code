﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeskTweaking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DeskTweaking))
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.TaskPreview = New System.Windows.Forms.Panel()
        Me.TopBar = New System.Windows.Forms.Panel()
        Me.TopBarBtn = New System.Windows.Forms.Button()
        Me.Desk = New System.Windows.Forms.Panel()
        Me.Tray = New System.Windows.Forms.Panel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.AppButton = New System.Windows.Forms.PictureBox()
        Me.OpenTaskbarImageDialog = New System.Windows.Forms.OpenFileDialog()
        Me.OpenAppButtonImageDialog = New System.Windows.Forms.OpenFileDialog()
        Me.OpenTopBarImageDialog = New System.Windows.Forms.OpenFileDialog()
        Me.OpenTopBarButtonImageDialog = New System.Windows.Forms.OpenFileDialog()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.OpenTrayImageDialog = New System.Windows.Forms.OpenFileDialog()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel9.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TaskPreview.SuspendLayout()
        Me.TopBar.SuspendLayout()
        Me.Desk.SuspendLayout()
        Me.Tray.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AppButton, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.White
        Me.Panel9.Controls.Add(Me.Panel10)
        Me.Panel9.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel9.Location = New System.Drawing.Point(5, 293)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(638, 5)
        Me.Panel9.TabIndex = 18
        '
        'Panel10
        '
        Me.Panel10.Location = New System.Drawing.Point(0, 435)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(5, 5)
        Me.Panel10.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Panel8)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(5, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(638, 34)
        Me.Panel1.TabIndex = 15
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.MinimizeNew
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox3.Location = New System.Drawing.Point(545, 4)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(28, 28)
        Me.PictureBox3.TabIndex = 19
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.MaximizeNew
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox2.Location = New System.Drawing.Point(576, 4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(28, 28)
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.CloseNew
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(607, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(28, 28)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(638, 5)
        Me.Panel8.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(101, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(437, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Desk Tweaking"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(643, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(5, 298)
        Me.Panel2.TabIndex = 16
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.Panel4.Location = New System.Drawing.Point(0, 293)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(5, 5)
        Me.Panel4.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(5, 5)
        Me.Panel3.TabIndex = 2
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Controls.Add(Me.Panel6)
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(5, 298)
        Me.Panel5.TabIndex = 17
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel6.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me.Panel6.Location = New System.Drawing.Point(0, 293)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(5, 5)
        Me.Panel6.TabIndex = 3
        '
        'Panel7
        '
        Me.Panel7.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(5, 5)
        Me.Panel7.TabIndex = 2
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.White
        Me.Panel11.Controls.Add(Me.Button3)
        Me.Panel11.Controls.Add(Me.Button2)
        Me.Panel11.Controls.Add(Me.Button1)
        Me.Panel11.Controls.Add(Me.PictureBox4)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel11.Location = New System.Drawing.Point(5, 34)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(638, 51)
        Me.Panel11.TabIndex = 19
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(165, 7)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(131, 37)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "Desk Tweaking"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(144, 7)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(25, 37)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = ">"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(61, 7)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 37)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Settings"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Back
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.Location = New System.Drawing.Point(8, 5)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(46, 41)
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(552, 163)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(60, 30)
        Me.Button15.TabIndex = 30
        Me.Button15.Text = "Stretch"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(493, 163)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(53, 30)
        Me.Button16.TabIndex = 29
        Me.Button16.Text = "Tile"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(294, 199)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(193, 30)
        Me.Button18.TabIndex = 28
        Me.Button18.Text = "Change Top Bar Button image..."
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(294, 163)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(193, 30)
        Me.Button17.TabIndex = 27
        Me.Button17.Text = "Change Top Bar image..."
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(294, 127)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(193, 30)
        Me.Button13.TabIndex = 24
        Me.Button13.Text = "Change App Button image..."
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(552, 91)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(60, 30)
        Me.Button10.TabIndex = 23
        Me.Button10.Text = "Stretch"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(493, 91)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(53, 30)
        Me.Button9.TabIndex = 22
        Me.Button9.Text = "Tile"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(294, 91)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(193, 30)
        Me.Button8.TabIndex = 21
        Me.Button8.Text = "Change Taskbar image..."
        Me.Button8.UseVisualStyleBackColor = True
        '
        'TaskPreview
        '
        Me.TaskPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.TaskPreview.Controls.Add(Me.TopBar)
        Me.TaskPreview.Controls.Add(Me.Desk)
        Me.TaskPreview.Location = New System.Drawing.Point(13, 91)
        Me.TaskPreview.Name = "TaskPreview"
        Me.TaskPreview.Size = New System.Drawing.Size(275, 188)
        Me.TaskPreview.TabIndex = 20
        '
        'TopBar
        '
        Me.TopBar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TopBar.BackColor = System.Drawing.Color.Transparent
        Me.TopBar.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.TopBar.Controls.Add(Me.TopBarBtn)
        Me.TopBar.Location = New System.Drawing.Point(100, 0)
        Me.TopBar.Name = "TopBar"
        Me.TopBar.Size = New System.Drawing.Size(74, 32)
        Me.TopBar.TabIndex = 2
        '
        'TopBarBtn
        '
        Me.TopBarBtn.BackColor = System.Drawing.Color.Transparent
        Me.TopBarBtn.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Menu
        Me.TopBarBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.TopBarBtn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TopBarBtn.FlatAppearance.BorderSize = 0
        Me.TopBarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke
        Me.TopBarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.TopBarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TopBarBtn.ForeColor = System.Drawing.Color.Transparent
        Me.TopBarBtn.Location = New System.Drawing.Point(0, 0)
        Me.TopBarBtn.Name = "TopBarBtn"
        Me.TopBarBtn.Size = New System.Drawing.Size(74, 32)
        Me.TopBarBtn.TabIndex = 1
        Me.TopBarBtn.UseVisualStyleBackColor = False
        '
        'Desk
        '
        Me.Desk.BackColor = System.Drawing.Color.Transparent
        Me.Desk.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Desk.Controls.Add(Me.Tray)
        Me.Desk.Controls.Add(Me.AppButton)
        Me.Desk.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Desk.Location = New System.Drawing.Point(0, 152)
        Me.Desk.Name = "Desk"
        Me.Desk.Size = New System.Drawing.Size(275, 36)
        Me.Desk.TabIndex = 1
        '
        'Tray
        '
        Me.Tray.Controls.Add(Me.PictureBox5)
        Me.Tray.Dock = System.Windows.Forms.DockStyle.Right
        Me.Tray.Location = New System.Drawing.Point(239, 0)
        Me.Tray.Name = "Tray"
        Me.Tray.Size = New System.Drawing.Size(36, 36)
        Me.Tray.TabIndex = 2
        '
        'PictureBox5
        '
        Me.PictureBox5.BackgroundImage = CType(resources.GetObject("PictureBox5.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox5.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(36, 36)
        Me.PictureBox5.TabIndex = 0
        Me.PictureBox5.TabStop = False
        '
        'AppButton
        '
        Me.AppButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.AppButton.Dock = System.Windows.Forms.DockStyle.Left
        Me.AppButton.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Taskbar_logo_new
        Me.AppButton.Location = New System.Drawing.Point(0, 0)
        Me.AppButton.Name = "AppButton"
        Me.AppButton.Size = New System.Drawing.Size(53, 36)
        Me.AppButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.AppButton.TabIndex = 1
        Me.AppButton.TabStop = False
        '
        'OpenTaskbarImageDialog
        '
        Me.OpenTaskbarImageDialog.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenTaskbarImageDialog.InitialDirectory = "C:\Users\"
        '
        'OpenAppButtonImageDialog
        '
        Me.OpenAppButtonImageDialog.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenAppButtonImageDialog.InitialDirectory = "C:\Users\"
        '
        'OpenTopBarImageDialog
        '
        Me.OpenTopBarImageDialog.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenTopBarImageDialog.InitialDirectory = "C:\Users\"
        '
        'OpenTopBarButtonImageDialog
        '
        Me.OpenTopBarButtonImageDialog.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenTopBarButtonImageDialog.InitialDirectory = "C:\Users\"
        '
        'Button11
        '
        Me.Button11.Enabled = False
        Me.Button11.Location = New System.Drawing.Point(570, 127)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(67, 30)
        Me.Button11.TabIndex = 26
        Me.Button11.Text = "Stretch"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Enabled = False
        Me.Button12.Location = New System.Drawing.Point(493, 127)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(71, 30)
        Me.Button12.TabIndex = 25
        Me.Button12.Text = "Center"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(294, 235)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(193, 30)
        Me.Button4.TabIndex = 31
        Me.Button4.Text = "Change Tray image..."
        Me.Button4.UseVisualStyleBackColor = True
        '
        'OpenTrayImageDialog
        '
        Me.OpenTrayImageDialog.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenTrayImageDialog.InitialDirectory = "C:\Users\"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(552, 235)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(60, 30)
        Me.Button5.TabIndex = 33
        Me.Button5.Text = "Stretch"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(493, 235)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(53, 30)
        Me.Button6.TabIndex = 32
        Me.Button6.Text = "Tile"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'DeskTweaking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(648, 298)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.TaskPreview)
        Me.Controls.Add(Me.Panel11)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DeskTweaking"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Desk Tweaking"
        Me.Panel9.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TaskPreview.ResumeLayout(False)
        Me.TopBar.ResumeLayout(False)
        Me.Desk.ResumeLayout(False)
        Me.Tray.ResumeLayout(False)
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AppButton, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents TaskPreview As System.Windows.Forms.Panel
    Friend WithEvents TopBar As System.Windows.Forms.Panel
    Friend WithEvents TopBarBtn As System.Windows.Forms.Button
    Friend WithEvents Desk As System.Windows.Forms.Panel
    Friend WithEvents AppButton As System.Windows.Forms.PictureBox
    Friend WithEvents Tray As System.Windows.Forms.Panel
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents OpenTaskbarImageDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenAppButtonImageDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenTopBarImageDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenTopBarButtonImageDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents OpenTrayImageDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
