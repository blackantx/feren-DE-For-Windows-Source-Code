﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings31Background
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Settings31Background))
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OpenWallpaperDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Wallpaper = New System.Windows.Forms.Panel()
        Me.colorwallpaper = New System.Windows.Forms.Panel()
        Me.PictureBox64 = New System.Windows.Forms.PictureBox()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.PictureBox63 = New System.Windows.Forms.PictureBox()
        Me.PictureBox55 = New System.Windows.Forms.PictureBox()
        Me.PictureBox62 = New System.Windows.Forms.PictureBox()
        Me.PictureBox56 = New System.Windows.Forms.PictureBox()
        Me.PictureBox61 = New System.Windows.Forms.PictureBox()
        Me.PictureBox57 = New System.Windows.Forms.PictureBox()
        Me.PictureBox60 = New System.Windows.Forms.PictureBox()
        Me.PictureBox58 = New System.Windows.Forms.PictureBox()
        Me.PictureBox59 = New System.Windows.Forms.PictureBox()
        Me.zoswallpaper = New System.Windows.Forms.Panel()
        Me.PictureBox54 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox53 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox52 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox50 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.PictureBox49 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox48 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox47 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox46 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox45 = New System.Windows.Forms.PictureBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox44 = New System.Windows.Forms.PictureBox()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.customwallpaper = New System.Windows.Forms.Panel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.PictureBox16 = New System.Windows.Forms.Panel()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StretchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ZoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1.SuspendLayout()
        Me.Wallpaper.SuspendLayout()
        Me.colorwallpaper.SuspendLayout()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zoswallpaper.SuspendLayout()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.customwallpaper.SuspendLayout()
        Me.PictureBox16.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer))
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(2, 498)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(714, 2)
        Me.Panel4.TabIndex = 15
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer))
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(716, 39)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(2, 461)
        Me.Panel3.TabIndex = 14
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer))
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 39)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(2, 461)
        Me.Panel2.TabIndex = 13
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(11, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(718, 39)
        Me.Panel1.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(640, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(18, 20)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "˅"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(663, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(18, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "˄"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Webdings", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(687, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "r"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(14, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Desk Background"
        '
        'OpenWallpaperDialog1
        '
        Me.OpenWallpaperDialog1.FileName = "wallpaper"
        Me.OpenWallpaperDialog1.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenWallpaperDialog1.InitialDirectory = "C:\Users\"
        '
        'Wallpaper
        '
        Me.Wallpaper.Controls.Add(Me.zoswallpaper)
        Me.Wallpaper.Controls.Add(Me.customwallpaper)
        Me.Wallpaper.Controls.Add(Me.Label15)
        Me.Wallpaper.Controls.Add(Me.Label13)
        Me.Wallpaper.Controls.Add(Me.Label14)
        Me.Wallpaper.Controls.Add(Me.colorwallpaper)
        Me.Wallpaper.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Wallpaper.Location = New System.Drawing.Point(2, 39)
        Me.Wallpaper.Name = "Wallpaper"
        Me.Wallpaper.Size = New System.Drawing.Size(714, 459)
        Me.Wallpaper.TabIndex = 16
        '
        'colorwallpaper
        '
        Me.colorwallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.colorwallpaper.Controls.Add(Me.PictureBox64)
        Me.colorwallpaper.Controls.Add(Me.PictureBox43)
        Me.colorwallpaper.Controls.Add(Me.PictureBox63)
        Me.colorwallpaper.Controls.Add(Me.PictureBox55)
        Me.colorwallpaper.Controls.Add(Me.PictureBox62)
        Me.colorwallpaper.Controls.Add(Me.PictureBox56)
        Me.colorwallpaper.Controls.Add(Me.PictureBox61)
        Me.colorwallpaper.Controls.Add(Me.PictureBox57)
        Me.colorwallpaper.Controls.Add(Me.PictureBox60)
        Me.colorwallpaper.Controls.Add(Me.PictureBox58)
        Me.colorwallpaper.Controls.Add(Me.PictureBox59)
        Me.colorwallpaper.Location = New System.Drawing.Point(28, 56)
        Me.colorwallpaper.Name = "colorwallpaper"
        Me.colorwallpaper.Size = New System.Drawing.Size(674, 391)
        Me.colorwallpaper.TabIndex = 10
        '
        'PictureBox64
        '
        Me.PictureBox64.BackColor = System.Drawing.SystemColors.Highlight
        Me.PictureBox64.Location = New System.Drawing.Point(144, 76)
        Me.PictureBox64.Name = "PictureBox64"
        Me.PictureBox64.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox64.TabIndex = 10
        Me.PictureBox64.TabStop = False
        '
        'PictureBox43
        '
        Me.PictureBox43.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox43.Location = New System.Drawing.Point(14, 16)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox43.TabIndex = 0
        Me.PictureBox43.TabStop = False
        '
        'PictureBox63
        '
        Me.PictureBox63.BackColor = System.Drawing.Color.HotPink
        Me.PictureBox63.Location = New System.Drawing.Point(79, 76)
        Me.PictureBox63.Name = "PictureBox63"
        Me.PictureBox63.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox63.TabIndex = 9
        Me.PictureBox63.TabStop = False
        '
        'PictureBox55
        '
        Me.PictureBox55.BackColor = System.Drawing.Color.Red
        Me.PictureBox55.Location = New System.Drawing.Point(79, 16)
        Me.PictureBox55.Name = "PictureBox55"
        Me.PictureBox55.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox55.TabIndex = 1
        Me.PictureBox55.TabStop = False
        '
        'PictureBox62
        '
        Me.PictureBox62.BackColor = System.Drawing.Color.Purple
        Me.PictureBox62.Location = New System.Drawing.Point(14, 76)
        Me.PictureBox62.Name = "PictureBox62"
        Me.PictureBox62.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox62.TabIndex = 8
        Me.PictureBox62.TabStop = False
        '
        'PictureBox56
        '
        Me.PictureBox56.BackColor = System.Drawing.Color.Orange
        Me.PictureBox56.Location = New System.Drawing.Point(144, 16)
        Me.PictureBox56.Name = "PictureBox56"
        Me.PictureBox56.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox56.TabIndex = 2
        Me.PictureBox56.TabStop = False
        '
        'PictureBox61
        '
        Me.PictureBox61.BackColor = System.Drawing.Color.DodgerBlue
        Me.PictureBox61.Location = New System.Drawing.Point(469, 16)
        Me.PictureBox61.Name = "PictureBox61"
        Me.PictureBox61.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox61.TabIndex = 7
        Me.PictureBox61.TabStop = False
        '
        'PictureBox57
        '
        Me.PictureBox57.BackColor = System.Drawing.Color.Yellow
        Me.PictureBox57.Location = New System.Drawing.Point(209, 16)
        Me.PictureBox57.Name = "PictureBox57"
        Me.PictureBox57.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox57.TabIndex = 3
        Me.PictureBox57.TabStop = False
        '
        'PictureBox60
        '
        Me.PictureBox60.BackColor = System.Drawing.Color.SteelBlue
        Me.PictureBox60.Location = New System.Drawing.Point(404, 16)
        Me.PictureBox60.Name = "PictureBox60"
        Me.PictureBox60.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox60.TabIndex = 6
        Me.PictureBox60.TabStop = False
        '
        'PictureBox58
        '
        Me.PictureBox58.BackColor = System.Drawing.Color.YellowGreen
        Me.PictureBox58.Location = New System.Drawing.Point(274, 16)
        Me.PictureBox58.Name = "PictureBox58"
        Me.PictureBox58.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox58.TabIndex = 4
        Me.PictureBox58.TabStop = False
        '
        'PictureBox59
        '
        Me.PictureBox59.BackColor = System.Drawing.Color.OliveDrab
        Me.PictureBox59.Location = New System.Drawing.Point(339, 16)
        Me.PictureBox59.Name = "PictureBox59"
        Me.PictureBox59.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox59.TabIndex = 5
        Me.PictureBox59.TabStop = False
        '
        'zoswallpaper
        '
        Me.zoswallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.zoswallpaper.AutoScroll = True
        Me.zoswallpaper.Controls.Add(Me.PictureBox54)
        Me.zoswallpaper.Controls.Add(Me.PictureBox6)
        Me.zoswallpaper.Controls.Add(Me.PictureBox53)
        Me.zoswallpaper.Controls.Add(Me.PictureBox7)
        Me.zoswallpaper.Controls.Add(Me.PictureBox52)
        Me.zoswallpaper.Controls.Add(Me.PictureBox8)
        Me.zoswallpaper.Controls.Add(Me.PictureBox51)
        Me.zoswallpaper.Controls.Add(Me.PictureBox9)
        Me.zoswallpaper.Controls.Add(Me.PictureBox50)
        Me.zoswallpaper.Controls.Add(Me.PictureBox10)
        Me.zoswallpaper.Controls.Add(Me.PictureBox49)
        Me.zoswallpaper.Controls.Add(Me.PictureBox11)
        Me.zoswallpaper.Controls.Add(Me.PictureBox48)
        Me.zoswallpaper.Controls.Add(Me.PictureBox12)
        Me.zoswallpaper.Controls.Add(Me.PictureBox47)
        Me.zoswallpaper.Controls.Add(Me.PictureBox13)
        Me.zoswallpaper.Controls.Add(Me.PictureBox46)
        Me.zoswallpaper.Controls.Add(Me.PictureBox14)
        Me.zoswallpaper.Controls.Add(Me.PictureBox45)
        Me.zoswallpaper.Controls.Add(Me.PictureBox15)
        Me.zoswallpaper.Controls.Add(Me.PictureBox44)
        Me.zoswallpaper.Controls.Add(Me.PictureBox18)
        Me.zoswallpaper.Location = New System.Drawing.Point(28, 56)
        Me.zoswallpaper.Name = "zoswallpaper"
        Me.zoswallpaper.Size = New System.Drawing.Size(674, 391)
        Me.zoswallpaper.TabIndex = 8
        '
        'PictureBox54
        '
        Me.PictureBox54.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox54.BackgroundImage = CType(resources.GetObject("PictureBox54.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox54.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox54.Location = New System.Drawing.Point(15, 683)
        Me.PictureBox54.Name = "PictureBox54"
        Me.PictureBox54.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox54.TabIndex = 21
        Me.PictureBox54.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox6.Location = New System.Drawing.Point(14, 17)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(138, 105)
        Me.PictureBox6.TabIndex = 0
        Me.PictureBox6.TabStop = False
        '
        'PictureBox53
        '
        Me.PictureBox53.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox53.BackgroundImage = CType(resources.GetObject("PictureBox53.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox53.Location = New System.Drawing.Point(1239, 572)
        Me.PictureBox53.Name = "PictureBox53"
        Me.PictureBox53.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox53.TabIndex = 20
        Me.PictureBox53.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.BackgroundImage = CType(resources.GetObject("PictureBox7.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox7.Location = New System.Drawing.Point(158, 17)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox7.TabIndex = 1
        Me.PictureBox7.TabStop = False
        '
        'PictureBox52
        '
        Me.PictureBox52.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox52.BackgroundImage = CType(resources.GetObject("PictureBox52.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox52.Location = New System.Drawing.Point(1086, 572)
        Me.PictureBox52.Name = "PictureBox52"
        Me.PictureBox52.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox52.TabIndex = 19
        Me.PictureBox52.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox8.BackgroundImage = CType(resources.GetObject("PictureBox8.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox8.Location = New System.Drawing.Point(311, 17)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox8.TabIndex = 2
        Me.PictureBox8.TabStop = False
        '
        'PictureBox51
        '
        Me.PictureBox51.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox51.BackgroundImage = CType(resources.GetObject("PictureBox51.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox51.Location = New System.Drawing.Point(933, 572)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox51.TabIndex = 18
        Me.PictureBox51.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox9.BackgroundImage = CType(resources.GetObject("PictureBox9.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox9.Location = New System.Drawing.Point(14, 128)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(168, 105)
        Me.PictureBox9.TabIndex = 3
        Me.PictureBox9.TabStop = False
        '
        'PictureBox50
        '
        Me.PictureBox50.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox50.BackgroundImage = CType(resources.GetObject("PictureBox50.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox50.Location = New System.Drawing.Point(780, 572)
        Me.PictureBox50.Name = "PictureBox50"
        Me.PictureBox50.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox50.TabIndex = 17
        Me.PictureBox50.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox10.BackgroundImage = CType(resources.GetObject("PictureBox10.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox10.Location = New System.Drawing.Point(188, 128)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(149, 105)
        Me.PictureBox10.TabIndex = 4
        Me.PictureBox10.TabStop = False
        '
        'PictureBox49
        '
        Me.PictureBox49.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox49.BackgroundImage = CType(resources.GetObject("PictureBox49.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox49.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox49.Location = New System.Drawing.Point(627, 572)
        Me.PictureBox49.Name = "PictureBox49"
        Me.PictureBox49.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox49.TabIndex = 16
        Me.PictureBox49.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.BackgroundImage = CType(resources.GetObject("PictureBox11.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox11.Location = New System.Drawing.Point(14, 239)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox11.TabIndex = 5
        Me.PictureBox11.TabStop = False
        '
        'PictureBox48
        '
        Me.PictureBox48.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox48.BackgroundImage = CType(resources.GetObject("PictureBox48.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox48.Location = New System.Drawing.Point(474, 572)
        Me.PictureBox48.Name = "PictureBox48"
        Me.PictureBox48.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox48.TabIndex = 15
        Me.PictureBox48.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.BackgroundImage = CType(resources.GetObject("PictureBox12.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox12.Location = New System.Drawing.Point(177, 239)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox12.TabIndex = 6
        Me.PictureBox12.TabStop = False
        '
        'PictureBox47
        '
        Me.PictureBox47.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox47.BackgroundImage = CType(resources.GetObject("PictureBox47.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox47.Location = New System.Drawing.Point(321, 572)
        Me.PictureBox47.Name = "PictureBox47"
        Me.PictureBox47.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox47.TabIndex = 14
        Me.PictureBox47.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox13.BackgroundImage = CType(resources.GetObject("PictureBox13.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox13.Location = New System.Drawing.Point(340, 239)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox13.TabIndex = 7
        Me.PictureBox13.TabStop = False
        '
        'PictureBox46
        '
        Me.PictureBox46.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox46.BackgroundImage = CType(resources.GetObject("PictureBox46.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox46.Location = New System.Drawing.Point(168, 572)
        Me.PictureBox46.Name = "PictureBox46"
        Me.PictureBox46.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox46.TabIndex = 13
        Me.PictureBox46.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox14.BackgroundImage = CType(resources.GetObject("PictureBox14.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox14.Location = New System.Drawing.Point(503, 239)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox14.TabIndex = 8
        Me.PictureBox14.TabStop = False
        '
        'PictureBox45
        '
        Me.PictureBox45.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox45.BackgroundImage = CType(resources.GetObject("PictureBox45.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox45.Location = New System.Drawing.Point(15, 572)
        Me.PictureBox45.Name = "PictureBox45"
        Me.PictureBox45.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox45.TabIndex = 12
        Me.PictureBox45.TabStop = False
        '
        'PictureBox15
        '
        Me.PictureBox15.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox15.BackgroundImage = CType(resources.GetObject("PictureBox15.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox15.Location = New System.Drawing.Point(15, 350)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox15.TabIndex = 9
        Me.PictureBox15.TabStop = False
        '
        'PictureBox44
        '
        Me.PictureBox44.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox44.BackgroundImage = CType(resources.GetObject("PictureBox44.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox44.Location = New System.Drawing.Point(464, 17)
        Me.PictureBox44.Name = "PictureBox44"
        Me.PictureBox44.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox44.TabIndex = 11
        Me.PictureBox44.TabStop = False
        '
        'PictureBox18
        '
        Me.PictureBox18.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox18.BackgroundImage = CType(resources.GetObject("PictureBox18.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox18.Location = New System.Drawing.Point(15, 461)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(194, 105)
        Me.PictureBox18.TabIndex = 10
        Me.PictureBox18.TabStop = False
        '
        'customwallpaper
        '
        Me.customwallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.customwallpaper.Controls.Add(Me.Button6)
        Me.customwallpaper.Controls.Add(Me.PictureBox16)
        Me.customwallpaper.Location = New System.Drawing.Point(28, 56)
        Me.customwallpaper.Name = "customwallpaper"
        Me.customwallpaper.Size = New System.Drawing.Size(674, 391)
        Me.customwallpaper.TabIndex = 9
        '
        'Button6
        '
        Me.Button6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button6.BackColor = System.Drawing.Color.Gainsboro
        Me.Button6.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue
        Me.Button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(5, 360)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(665, 27)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "Use your Windows desktop background. (MUST NOT BE SLIDESHOW!!!)"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'PictureBox16
        '
        Me.PictureBox16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox16.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox16.BackgroundImage = CType(resources.GetObject("PictureBox16.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox16.Controls.Add(Me.PictureBox17)
        Me.PictureBox16.Location = New System.Drawing.Point(5, 5)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(665, 350)
        Me.PictureBox16.TabIndex = 2
        '
        'PictureBox17
        '
        Me.PictureBox17.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox17.BackgroundImage = CType(resources.GetObject("PictureBox17.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox17.ContextMenuStrip = Me.ContextMenuStrip1
        Me.PictureBox17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox17.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(665, 350)
        Me.PictureBox17.TabIndex = 0
        Me.PictureBox17.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Silver
        Me.Label15.Location = New System.Drawing.Point(386, 18)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(144, 28)
        Me.Label15.TabIndex = 13
        Me.Label15.Text = "custom colour"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Silver
        Me.Label13.Location = New System.Drawing.Point(231, 18)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(151, 28)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "custom picture"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(29, 18)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(197, 28)
        Me.Label14.TabIndex = 11
        Me.Label14.Text = "zuaro os wallpapers"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StretchToolStripMenuItem, Me.ZoomToolStripMenuItem, Me.TileToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(112, 70)
        '
        'StretchToolStripMenuItem
        '
        Me.StretchToolStripMenuItem.Name = "StretchToolStripMenuItem"
        Me.StretchToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.StretchToolStripMenuItem.Text = "Stretch"
        '
        'ZoomToolStripMenuItem
        '
        Me.ZoomToolStripMenuItem.Name = "ZoomToolStripMenuItem"
        Me.ZoomToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.ZoomToolStripMenuItem.Text = "Zoom"
        '
        'TileToolStripMenuItem
        '
        Me.TileToolStripMenuItem.Name = "TileToolStripMenuItem"
        Me.TileToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.TileToolStripMenuItem.Text = "Tile"
        '
        'Settings31Background
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(718, 500)
        Me.Controls.Add(Me.Wallpaper)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Settings31Background"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Desk Background"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Wallpaper.ResumeLayout(False)
        Me.Wallpaper.PerformLayout()
        Me.colorwallpaper.ResumeLayout(False)
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zoswallpaper.ResumeLayout(False)
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.customwallpaper.ResumeLayout(False)
        Me.PictureBox16.ResumeLayout(False)
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OpenWallpaperDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Wallpaper As System.Windows.Forms.Panel
    Friend WithEvents customwallpaper As System.Windows.Forms.Panel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents PictureBox16 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents zoswallpaper As System.Windows.Forms.Panel
    Friend WithEvents PictureBox54 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox53 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox52 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox51 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox50 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox49 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox48 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox47 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox46 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox45 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox44 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents colorwallpaper As System.Windows.Forms.Panel
    Friend WithEvents PictureBox64 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox43 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox63 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox55 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox62 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox56 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox61 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox57 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox60 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox58 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox59 As System.Windows.Forms.PictureBox
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents StretchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ZoomToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
